package demo01;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;

import figures.PointInterface;
import spring.Config;

public class ClassTest01 {
	public static void main(String[] args) {
	  
	  AbstractApplicationContext context = new AnnotationConfigApplicationContext(Config.class);
	  PointInterface point = (PointInterface) context.getBean("point");

      System.out.println("---- (1) ----");
      
      point.setX(20);
      
      System.out.println("---- (2) ----");
      
      point.setY(100);
      
      System.out.println("---- (3) ----");
  }
}
