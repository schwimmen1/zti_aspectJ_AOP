package demo01;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;

@Aspect
public class AspectJ01 {

	@Before("execution(void figures.Point.setX(int))")
	public void setX() {
		System.out.println("Before call Point.setX(int)");
	}
}
