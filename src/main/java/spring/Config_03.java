package spring;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

import demo03.AspectJ03;
import figures.FigureElement;
import figures.Line;
import figures.Point;

@Configuration
@EnableAspectJAutoProxy
public class Config_03 {

	@Bean(name = "point")
	public Point getPoint() {
		return new Point(10, 200);
	}
	
	@Bean(name = "line")
	public FigureElement getLine() {
		return new Line(new Point(1,1), new Point(10,10));
	}
	
	@Bean
	public AspectJ03 getAspectJ03() {
		return new AspectJ03();
	}
	
}
