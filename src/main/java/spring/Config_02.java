package spring;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

import demo02.AspectJ02;
import figures.Point;

@Configuration
@EnableAspectJAutoProxy
public class Config_02 {

	@Bean(name = "point")
	public Point getPoint() {
		return new Point(10, 200);
	}
	
	@Bean
	public AspectJ02 getAspectJ02() {
		return new AspectJ02();
	}
	
}
