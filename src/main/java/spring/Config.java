package spring;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

import demo01.AspectJ01;
import figures.Point;

@Configuration
@EnableAspectJAutoProxy
public class Config {

	@Bean(name = "point")
	public Point getPoint() {
		return new Point(10, 200);
	}
	
	@Bean
	public AspectJ01 getAspectJ01() {
		return new AspectJ01();
	}
	
}
