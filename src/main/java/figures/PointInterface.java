package figures;

public interface PointInterface {

	public int getX();
	public int getY();
	public void setX(int x);
	public void setY(int y);
	public void move(int dx, int dy);
}
