package demo02;
 
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;

import figures.Point;
import figures.PointInterface;
import spring.Config;
import spring.Config_02;
 
public class ClassTest02 {
 
  public static void main(String[] args) {
 
	  AbstractApplicationContext context = new AnnotationConfigApplicationContext(Config_02.class);
	  PointInterface point = (PointInterface) context.getBean("point");

      System.out.println("---- (1) ----");
 
      point.move(20, 30);
 
      System.out.println("---- (2) ----");
 
      System.out.println(point.toString());
 
      System.out.println("---- (3) ----");
 
      point.setX(100);
  }
 
}