package demo02;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;

import figures.Point;

@Aspect
public class AspectJ02 {  

	@Before("execution(* figures.Point.move(int,int)) && args(dx,dy) && target(point)")
	public void moveAction(Point point, int dx, int dy) {
		System.out.println("Before call move(" + dx + "," + dy + ")");
		System.out.println(point.toString());
	}
}
