package demo03;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;

@Aspect
public class AspectJ03 {  

	@Pointcut("execution(void figures.Point.setX(int))")
	public void getX() {
		
	}
	
	@Pointcut("execution(void figures.Point.setY(int))")
	public void getY() {
		
	}
	
	@Pointcut("execution(void figures.FigureElement.move(int,int))")
	public void move() {
		
	}
	
	//@Before("execution(void figures.Point.setX(int)) || execution(void figures.Point.setY(int)) || execution(void figures.FigureElement.move(int,int)) ")
	@Before("getX() || getY() || move()")
	public void moveAction() {
		System.out.println("before move");
	}
}
