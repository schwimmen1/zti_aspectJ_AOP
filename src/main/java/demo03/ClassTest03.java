package demo03;
 
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;

import figures.FigureElement;
import figures.PointInterface;
import spring.Config_03;
 
public class ClassTest03 {
  public static void main(String[] args) {
 
	  AbstractApplicationContext context = new AnnotationConfigApplicationContext(Config_03.class);
	  PointInterface point = (PointInterface) context.getBean("point");
 
      System.out.println("---- (1) ----");
 
      point.setX(20);
 
      System.out.println("---- (2) ----");
 
      FigureElement line = (FigureElement) context.getBean("line");
      
      line.move(10, 10);
 
      System.out.println("---- (3) ----");
  }
}